.. _fenics-adjoint-documentation:

.. title:: fenics-adjoint documentation

******************************************
Documentation for fenics-adjoint |version|
******************************************

Quickstart
==========

Have you just discovered fenics-adjoint and want to get up and running
quickly? Then, we suggest you

 * Start by going through the first steps of the
   :ref:`fenics-adjoint tutorial <fenics-adjoint-tutorial>`.
 * Move on to look at the more detailed :ref:`Stokes Dirichlet BC control example <stokes-bc-control-example>`.
 * Install :ref:`dolfin-adjoint and its dependencies <download>`.

Next, feel free to take a look at the complete :ref:`introduction to
fenics-adjoint <fenics-adjoint-manual>`, more :ref:`fenics-adjoint
examples <fenics-adjoint-examples>`, an introduction to the
:ref:`mathematical background of adjoints
<fenics-adjoint-mathematical-background>`,
or the :ref:`fenics-adjoint <fenics-adjoint-api-reference>`
and :ref:`pyadjoint <pyadjoint-api-reference>` API references.

Resources
=========

.. raw:: html

  <div id="main">
  <div id="container" class="feature">
    <div id="content">
      <div id="sub-feature">
	<div id="front-block-1" class="front-block block">
          <h2> dolfin-adjoint manual </h2>

The :ref:`fenics-adjoint manual <fenics-adjoint-manual>` describes
how to use fenics-adjoint and the main features of fenics-adjoint, and
includes introductory examples. For more use cases, see the
:ref:`examples <fenics-adjoint-examples>`.

.. only:: html

  There is also a `pdf version <../_static/dolfin-adjoint-all.pdf>`_
  of the documentation available.

.. raw:: html

  <h2>The mathematical background</h2>

Are you not that familiar with adjoints? Adjoints show up in many
applications, and in many computational techniques. You might be
interested in a :ref:`mathematical introduction to adjoints and
their applications <fenics-adjoint-mathematical-background>`.


.. raw:: html

  </div><!-- #front-block-1 .front-block .block-->

  <div id="front-block-2" class="front-block block">

  <h2>Examples of fenics-adjoint usage</h2>

We have implemented and documented a number of :ref:`examples <fenics-adjoint-examples>` using fenics-adjoint.

Many more examples of dolfin-adjoint usage can be found in the
separate `dolfin-adjoint applications repository
<http://bitbucket.org/dolfin-adjoint/da-applications>`__ on Bitbucket.

.. raw:: html

  <h2>fenics-adjoint API reference</h2>

Are you looking for the complete reference documentation? Look no
further, it is right here: :ref:`fenics-adjoint API reference
<fenics-adjoint-api-reference>`, :ref:`pyadjoint API reference <pyadjoint-api-reference>`.



.. raw:: html

   </div><!-- #front-block-2 .front-block .block-->
   </div><!-- #sub-feature -->
     </div><!-- #content -->
       </div><!-- #container .feature -->
         </div><!-- #main -->


Frequently asked questions:
===========================

Some questions are asked more often than others: take a look at our
:ref:`Frequently asked questions <dolfin-adjoint-faq>` before
:ref:`seeking support <support>`.


.. toctree::
   :hidden:

   faq
